<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://gitlab.com/vlzdavid12/backend-nest-pqr/-/raw/main/screenshot.png"  /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest


## Description
Project backend PQR with Nest.js
## Installation


Install all the package with node.
```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Install Mongo DB
It is located in this project as docker-composer.yaml <br/>
Note: Install Docker Desktop [here](https://www.docker.com/products/docker-desktop/)
```bash
docker-composer up -d
```
[More information Docker Mongo](https://hub.docker.com/_/mongo) <br/>
[More information Config Nest.js](https://docs.nestjs.com/techniques/mongodb)

## Data Dummi
Protocolo GET | Do you have data for demonstration.
```
http://localhost:3000/api/seed
```

## Document Api 
```bash
http://localhost:3000/api
```
