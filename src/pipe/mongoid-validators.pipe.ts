import {ArgumentMetadata, BadRequestException, Injectable, PipeTransform} from '@nestjs/common';
import {isValidObjectId} from "mongoose";

@Injectable()
export class MongoidValidatorsPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if(!isValidObjectId(value)){
      throw  new BadRequestException(`This ${value} is not valid Mongo Id`);
    }

    return value;
  }
}
