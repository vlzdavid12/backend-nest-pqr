import { NestFactory } from '@nestjs/core';
import {ValidationPipe} from "@nestjs/common";
import { AppModule } from './app.module';
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";


async function main() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix("api");
  app.enableCors();
  app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        forbidNonWhitelisted: true
      })
  );

  const config =  new DocumentBuilder()
      .setTitle("Demo Backend PQR")
      .setDescription("This is description by Pqr")
      .setVersion('1.0')
      .addTag('PQR api')
      .build();
  const document =  SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}
main();
