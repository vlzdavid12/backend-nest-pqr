import {Injectable} from '@nestjs/common';
import {Model} from "mongoose";
import {Pqr} from "../pqr/entities/pqr.entity";
import {InjectModel} from "@nestjs/mongoose";
import {PostType} from "../pqr/dto/create-pqr.dto";

@Injectable()
export class SeedService {
    constructor(
        @InjectModel(Pqr.name)
        private readonly  pqrModel: Model<Pqr>
    ) {}
    async  startSeed() {

        await this.pqrModel.deleteMany({});
        const data = [
            {firstName: "Juan Fernando", lastName: "Estupiñan Caicedo", phone: "3234569789", email: "juand.est@gmail.com", type: PostType.PETICION, comments: "Nueva pregunta de demo servicio 1."},
            {firstName: "Andrea Stefa", lastName: "Calderon Ortis", phone: "3634586889", email: "calderon.est@hotmail.com", type: PostType.QUEJA, comments: "Nueva pregunta de demo servicio 2."},
            {firstName: "David Fernando", lastName: "Valenzuela Pardo", phone: "3547897089", email: "david12@outlook.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 3."},
            {firstName: "Edgar Julian", lastName: "Sombo Caballer", phone: "3547890909", email: "sombo1234@outlook.com", type: PostType.QUEJA, comments: "Nueva pregunta de demo servicio 4."},
            {firstName: "Alejandra Zamira", lastName: "Pinzon Penagos", phone: "3543789089", email: "penagos.zamy231@yahoo.com", type: PostType.PETICION, comments: "Nueva pregunta de demo servicio 5."},
            {firstName: "Fede Mamon", lastName: "Popo Zuela", phone: "3547456089", email: "popoZuela2132@gmail.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 6."},
            {firstName: "Titin Rock", lastName: "Pineda Melendez", phone: "3547123489", email: "david12@outlook.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 7."},
            {firstName: "Magarita Rosa", lastName: "Buenona Amores", phone: "34547589089", email: "rosa.amores@outlook.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 8."},
            {firstName: "Pilar Paloma", lastName: "Salvage Zalgar", phone: "23547689089", email: "zalgar.pilar@hotmail.com", type: PostType.PETICION, comments: "Nueva pregunta de demo servicio 9."},
            {firstName: "Natalia Sandra", lastName: "Mora Diaz", phone: "36548789089", email: "sandra.mora@gmail.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 10."},
            {firstName: "Tulio Alejo", lastName: "Trocha Falgar", phone: "31547789089", email: "trocha.alejo@outlook.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 12."},
            {firstName: "Sam Felipe", lastName: "Mosca Piedra", phone: "32478908945", email: "piedramosca83@outlook.com", type: PostType.QUEJA, comments: "Nueva pregunta de demo servicio 13."},
            {firstName: "Bombón Luna", lastName: "Bilbao Bazar", phone: "35478908990", email: "dbombon@outlook.com", type: PostType.PETICION, comments: "Nueva pregunta de demo servicio 14."},
            {firstName: "Burbuja Luu", lastName: "Nuñez Toro", phone: "25473890893", email: "burbuja.toro@gmail.com", type: PostType.QUEJA, comments: "Nueva pregunta de demo servicio 15."},
            {firstName: "Bellota Vanessa", lastName: "Morgan Kirby", phone: "35478908649", email: "bellota.Morgan.kirby@outlook.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 16."},
            {firstName: "Snoopy Doo", lastName: "Pomada Rey", phone: "35445890859", email: "pomada.rey@yahoo.com", type: PostType.RECLAMO, comments: "Nueva pregunta de demo servicio 17."},
            {firstName: "Pinky Steven", lastName: "Warner Mania", phone: "33478908967", email: "pinki.warner@outlook.com", type: PostType.PETICION, comments: "Nueva pregunta de demo servicio 18."},
        ];

        await this.pqrModel.insertMany(data);
        return "Data Insert Seed Success Fully";
    }
}
