import { Module } from '@nestjs/common';
import { SeedService } from './seed.service';
import { SeedController } from './seed.controller';
import {PqrModule} from "../pqr/pqr.module";

@Module({
  controllers: [SeedController],
  providers: [SeedService],
  imports:[
      PqrModule
  ]
})
export class SeedModule {}
