import { Module } from '@nestjs/common';
import { PqrModule } from './pqr/pqr.module';
import {MongooseModule} from "@nestjs/mongoose";
import { SeedModule } from './seed/seed.module';

@Module({
  imports: [
      MongooseModule.forRoot('mongodb://localhost:27017/nest-pqr'),
      PqrModule,
      SeedModule],
  controllers: [],
  providers: []
})
export class AppModule {}
