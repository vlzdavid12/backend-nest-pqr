import {Document} from "mongoose";
import {Schema, SchemaFactory, Prop} from "@nestjs/mongoose";

@Schema()
export class Pqr extends  Document{
    @Prop({type: String, required: true})
    firstName: string
    @Prop({type: String, required: true})
    lastName: string
    @Prop({type: Number })
    phone: number
    @Prop({type: String, required: true})
    email: string
    @Prop({type: String, required: true})
    type: string
    @Prop({type: String, required: true})
    comments: string
}

export const  PqrSchema =  SchemaFactory.createForClass(Pqr);
