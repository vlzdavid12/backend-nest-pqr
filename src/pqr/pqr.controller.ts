import {Controller, Get, Post, Body, Patch, Param, Delete, ParseUUIDPipe} from '@nestjs/common';
import { PqrService } from './pqr.service';
import { CreatePqrDto } from './dto/create-pqr.dto';
import { UpdatePqrDto } from './dto/update-pqr.dto';
import {ApiCreatedResponse, ApiForbiddenResponse, ApiResponse, ApiTags} from "@nestjs/swagger";
import {MongoidValidatorsPipe} from "../pipe/mongoid-validators.pipe";

@ApiTags("PQR CRUD")
@Controller('pqr')
export class PqrController {
  constructor(private readonly pqrService: PqrService) {}

  @Post()
  @ApiResponse({status: 200, description: "The pqr has been successfully created"})
  @ApiResponse({status: 400, description: "Bad Request..."})
  @ApiResponse({status: 403, description: "Forbidden..."})
  create(@Body() createPqrDto: CreatePqrDto) {
    return this.pqrService.create(createPqrDto);
  }

  @Get()
  findAll() {
    return this.pqrService.findAll();
  }

  @Get(':id')
  @ApiResponse({status: 200, description: "Success fully pqr by Id..."})
  @ApiResponse({status: 400, description: "Error BadRequest..."})
  findOne(@Param('id', MongoidValidatorsPipe) id: string) {
    return this.pqrService.findOne(id);
  }

  @Patch(':id')
  @ApiResponse({status: 200, description: "Success fully pqr by Id..."})
  @ApiResponse({status: 400, description: "Error BadRequest..."})
  update(@Param('id',MongoidValidatorsPipe) id: string, @Body() updatePqrDto: UpdatePqrDto) {
    return this.pqrService.update(id, updatePqrDto);
  }

  @Delete(':id')
  @ApiResponse({status: 200, description: "Success fully pqr by Id..."})
  @ApiResponse({status: 400, description: "Error BadRequest..."})
  remove(@Param('id', MongoidValidatorsPipe) id: string) {
    return this.pqrService.remove(id);
  }
}
