import {IsEmail, IsEnum, IsNumber, IsString, MinLength} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export enum PostType {
    RECLAMO = "RECLAMO",
    QUEJA = "QUEJA",
    PETICION = "PETICION"
}


export class CreatePqrDto {

    @ApiProperty()
    @IsString()
    @MinLength(1)
    firstName: string
    @ApiProperty()
    @IsString()
    @MinLength(1)
    lastName: string
    @ApiProperty()
    @IsNumber()
    phone: number
    @ApiProperty()
    @IsEmail()
    email: string
    @ApiProperty()
    @IsString()
    @IsEnum(PostType)
    type: string
    @ApiProperty()
    @IsString()
    @MinLength(1)
    comments: string
}
