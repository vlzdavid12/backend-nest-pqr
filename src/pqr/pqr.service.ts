import {BadRequestException, Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import { CreatePqrDto } from './dto/create-pqr.dto';
import { UpdatePqrDto } from './dto/update-pqr.dto';
import {Model} from "mongoose";
import {Pqr} from "./entities/pqr.entity";
import {InjectModel} from "@nestjs/mongoose";

@Injectable()
export class PqrService {

  constructor(
      @InjectModel(Pqr.name)
      private readonly  pqrModel: Model<Pqr>
  ) {
  }

  async create(createPqrDto: CreatePqrDto) {
      try{
        const pqr =  await this.pqrModel.create(createPqrDto);
        return pqr;
      }catch (error){
        this.handleExceptions(error);
      }
  }

  findAll() {
    return this.pqrModel.find();
  }

  findOne(id: string) {
   let pqr = this.pqrModel.findById(id);
   if(!pqr) throw  new NotFoundException("This Pqr not found...")
   return pqr;
  }

  async update(id: string, updatePqrDto: UpdatePqrDto) {
    const pqr =  await this.findOne(id);
    if(updatePqrDto.firstName){
      try{
        await pqr.updateOne(updatePqrDto);
        return {...pqr.toJSON(), ...updatePqrDto}
      }catch (error){
        this.handleExceptions(error);
      }
    }
  }

  async remove(id: string) {
   const {deletedCount} = await this.pqrModel.deleteOne({_id: id})
   if(deletedCount === 0){
     throw new BadRequestException(`Pqr not found with id: ${id}`)
   }
   return;
  }

  private handleExceptions(error: any) {
    throw new InternalServerErrorException("Error create PQR - Check server Logs");
  }
}
