import { Module } from '@nestjs/common';
import { PqrService } from './pqr.service';
import { PqrController } from './pqr.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {Pqr, PqrSchema} from "./entities/pqr.entity";

@Module({
  controllers: [PqrController],
  providers: [PqrService],
  imports: [
      MongooseModule.forFeature([
        {
          name: Pqr.name,
          schema: PqrSchema
        }
      ])
  ], exports:[
      MongooseModule
  ]
})
export class PqrModule {}
